# README #

Node app that runs on Epson DiscPublisher PC.

It receives pings from DiscPublishServer to download jobs from ServerApi, cancel jobs, or pause burning.

It downloads assets from ServerAPI, caches them, and creates the proper instructions to get Epson DiscPublisher to burn discs with merged labels.

### Flow of publishing a disc ###

Click “Publish Disc” Button on Web App → Save order to DB on Server-API, then send notification to → DiscPublish-Server which sends message to → DiscPublish-Agent to pull all queued jobs from → Server-API

- HTTP (socket.io or SignalR):
    - **on agent startup only**: 
        - Notify DiscPublish-Server with Agent status (Hi, I’m here if you need me!) 
        - Ask Server-API for all queued jobs. (What’s been piling up while I was away??)
    - DiscPublish-Server sends message as needed to tell Agent to ask Server-API for queued jobs. 
    - Server-API replies with list of queued jobs.
    - Agent saves jobs in memory and replies back with ‘pending’ status for those jobs. 
    - Agent checks cache, and asks server-API async for URLs to any files that are needed (not in cache), while starting any jobs that already have what they need.
    - Server-API replies with the URLs of needed files.
    - Agent downloads the files to cache.  
    - As files get downloaded, Agent writes JDFs and puts them in Hot Folder.
    - Update server-api with job and publisher status.
- IO:
    - Write files to cache.
    - Move JDF files from cache to hot folder.
    - Check files exist on Network path.
    - Get size of all files in cache.
    - Delete cache files based on created date.
- Other:
    - Run as service.
    - Auto-start before logging in.
- GUI:
    - Setting box to change Paths, Agent Name, max cache size

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact